package computerdatabase

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.language.postfixOps

class BasicSimulation extends Simulation {

  val httpProtocol = http
    .baseUrl("https://tst-dom.crv4all.com/customer") // ACC -> On-premise DB
    //    .baseUrl("http://bovaris-jbossas-07t.adinfra.crv4all.com:8080/customer") // TST -> Azure DB
    .acceptHeader("*/*") // Here are the common headers
    .acceptEncodingHeader("gzip, deflate, br")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")
    .authorizationHeader("TOKEN_HERE")

  val scn = scenario("Customer API") // A scenario is a chain of requests and pauses
    .repeat(50) {
      exec(http("Customer by id")
        .get("/customers/0030078b-2ed6-45e4-82ac-87e25cf300a1"))
        .exec(http("Customer locations by id")
          .get("/customers/0030078b-2ed6-45e4-82ac-87e25cf300a1/locations"))
        .exec(http("Mandates by customer id")
          .get("/customers/0030078b-2ed6-45e4-82ac-87e25cf300a1/mandates"))
        .exec(http("Support by customer id")
          .get("/customers/0030078b-2ed6-45e4-82ac-87e25cf300a1/support"))
        .exec(http("Subscriptions by customer id")
          .get("/customers/0030078b-2ed6-45e4-82ac-87e25cf300a1/subscriptions"))
        .exec(http("get all customers")
          .get("/customers?subscription-code=ANA"))
        .exec(http("get all locations")
          .get("/locations"))
        .exec(http("Location by id")
          .get("/locations/2e56e0be-7eb2-45d0-bf3a-333f10211eb3"))
    }


  setUp(scn.inject(atOnceUsers(1)).protocols(httpProtocol))
}
